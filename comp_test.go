package comp

import (
	"fmt"
	"testing"
	"time"
)

type CompPrinter struct{}

func (CompPrinter) Init(execArgs map[string]interface{}, args ...interface{}) error {
	fmt.Println("init, arguments:", args)
	return nil
}

func (CompPrinter) Start(args ...interface{}) error {
	fmt.Println("start, arguments:", args)
	return nil
}

func (CompPrinter) UnInit() {
	fmt.Println("unInit")
}

func (CompPrinter) Name() string {
	return "printer"
}

func TestComp(t *testing.T) {
	t.Run("test_comp", func(t *testing.T) {
		if err := RegComp(new(CompPrinter)); err != nil {
			t.Error(err)
			t.Fail()
		}

		if err := RegComp(new(CompPrinter)); err != ErrRepeatReg {
			t.Error("重复注册应该报错")
			t.Fail()
		}

		_ = Init(map[string]interface{}{"key":"value"}, 1,2,3, "hello", "world")

		ch := make(chan bool)
		go func() {
			<-time.After(time.Second * 10)
			ch<-true
		}()

		go func() {
			<-time.After(time.Second * 1)
			err := RegComp(new(CompPrinter))
			if err != ErrRunning {
				t.Error("运行期间注册成功")
				t.Fail()
			}
		}()

		go func() {
			ticker := time.NewTicker(time.Second)
			defer func() {
				ticker.Stop()
			}()

			for range ticker.C {
				p, ok := Comp("printer")
				if !ok {
					continue
				}

				p.Name()
			}
		}()

		_ = Start(ch, "foo", "bar")
	})
}
