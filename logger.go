package comp

var (
	logger Logger
)

type Logger interface {
	Debug(...interface{})
	Info(...interface{})
	Warn(...interface{})
	Error(...interface{})
	DebugF(string, ...interface{})
	InfoF(string, ...interface{})
	WarnF(string, ...interface{})
	ErrorF(string, ...interface{})
}

func RegLogger(l Logger) {
	logger = l
}