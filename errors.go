package comp

import "errors"

var (
	ErrRunning = errors.New("cannot operation in running")
	ErrRepeatReg = errors.New("repeat register")
)

