package comp

import (
	"os"
	"os/signal"
	"syscall"
)

var (
	components []Component
	index = make(map[string]int)
	running bool
)

// RegComp 注册组件
func RegComp(comp Component) error {
	if running {
		return ErrRunning
	}

	idx := len(components)
	components = append(components, comp)

	if _, ok := index[comp.Name()]; ok {
		return ErrRepeatReg
	}

	index[comp.Name()]=idx
	return nil
}

func forEach(fn func(comp Component) error) error {
	for _, comp := range components {
		if err := fn(comp); err != nil {
			return err
		}
	}

	return nil
}

func forEachReverse(fn func(comp Component) error) error {
	for i := len(components) - 1; i >= 0; i-- {
		comp := components[i]
		if err := fn(comp); err != nil {
			return err
		}
	}

	return nil
}

func Comp(name string) (Component,bool) {
	idx, ok := index[name]
	if !ok {
		return nil, ok
	}

	return components[idx], true
}

func Init(execArgs map[string]interface{}, args ...interface{}) error {
	logger.Info("开始加载模块.")
	err := forEach(func(comp Component) error {
		if err := comp.Init(execArgs, args...); err != nil {
			logger.Info("加载模块: >>", comp.Name(), "<< 失败: ", err)
			return err
		}

		logger.Info("加载模块: >>", comp.Name(), "<< 成功.")
		return nil
	})

	logger.Info("加载模块完成.")
	return err
}

func Start(chClose chan bool, args ...interface{}) error {
	defer func() {
		running = false
	}()

	err := forEach(func(comp Component) error {
		if err := comp.Start(args...); err != nil {
			logger.Info("装载模块: >>", comp.Name(), "<< 失败: ", err)
			return err
		}

		logger.Info("装载模块: >>", comp.Name(), "<< 成功.")
		return nil
	})

	if err != nil {
		return err
	}

	logger.Info("启动模块完成.")
	running = true

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	select {
	case s:=<-sigs:
		logger.Info("收到系统信号:", s, ", 卸载所有组件")
	case <-chClose:
		logger.Info("收到关闭信号, 卸载所有组件")
	}

	_ = forEachReverse(func(comp Component) error {
		comp.UnInit()
		logger.Info("卸载模块: >>", comp.Name(), "<< 完成.")
		return nil
	})

	return nil
}

type Component interface {
	// Init 初始化
	Init(map[string]interface{}, ...interface{}) error
	// Start 启动
	Start(...interface{}) error
	// UnInit 卸载
	UnInit()
	// Name 模块名称
	Name() string
}
